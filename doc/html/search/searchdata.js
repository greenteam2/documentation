var indexSectionsWithContent =
{
  0: "abcdfgikmnprstuw",
  1: "fpt",
  2: "w",
  3: "fp",
  4: "cdfgimprsu",
  5: "u",
  6: "iu",
  7: "abcdiknprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "enums",
  6: "enumvalues",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties"
};

